﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;



namespace ConsoleGameTetris
{
    class Program
    {
        public static int ROW = 25;
        public static int COL = 15;
        public static int initialPosition = 5, endingPosition;
        public static int m_iScore;
        public static int[,] Board=new int[ROW+10,COL+10];

        public static ConsoleColor OBJECT_COLOR = ConsoleColor.DarkBlue;
        public static ConsoleColor BACKGROUND_COLOR = ConsoleColor.Green;

        public static ObjectPattern Obj = new ObjectPattern(1,0); //Will represent our here that's moving around :P/>

        static void Main(string[] args)
        {
            InitializeGameBoard();
            DrawBoard();
            int iTemp = COL / 2;
            Obj = new ObjectPattern(1, 0)
            {
                X = initialPosition + 1,
                Y = initialPosition + iTemp
            };
            Obj.DisplayObject();

            GamePlay game = new GamePlay();
            game.RequestStart();
            Thread gameThread = new Thread(game.StartGame);
            gameThread.Start();

            ConsoleKeyInfo keyInfo;
            while ((keyInfo = Console.ReadKey(true)).Key != ConsoleKey.Escape)
            {
                switch (keyInfo.Key)
                {
                    case ConsoleKey.UpArrow:
                        //MoveObject(-1, 0);
                        //game.RequestStop();
                        Obj.ChangeDirection();
                        
                        break;

                    case ConsoleKey.RightArrow:
                        Obj.MoveCurrentObject(0, 1);
                        break;

                    case ConsoleKey.DownArrow:
                        Obj.MoveCurrentObject(1, 0);
                        break;

                    case ConsoleKey.LeftArrow:
                        Obj.MoveCurrentObject(0, -1);
                        break;
                }
            }
        }
        public static void InitializeGameBoard()
        {
            for(int i=0;i<ROW;i++)
                for(int j=0;j<COL;j++) 
                    Board[i,j]=0;
            for (int i = ROW, j = 0; j <= COL; j++)
                Board[i,j] = 1;
            for(int j=COL,i=0;i<=ROW;i++)
                Board[i,j]=1;


        }

        public static void DrawBoard()
        {
            SetBackgroundColor();
            Console.BackgroundColor = ConsoleColor.Blue;
            
            for (int i = initialPosition; i <initialPosition + ROW; i++)
            {
                Console.SetCursorPosition(initialPosition-1, i);
                Console.Write(" ");
                Console.SetCursorPosition(initialPosition + COL, i);
                Console.Write(" ");

            }
            for (int j = initialPosition; j < initialPosition + COL; j++ )
            {
                Console.SetCursorPosition(j, initialPosition + ROW);
                Console.Write(" ");

                Console.SetCursorPosition(j, initialPosition-1);
                Console.Write(" ");
            }

            Console.SetCursorPosition(initialPosition - 1, initialPosition - 1);
            Console.Write(" ");
            Console.SetCursorPosition(initialPosition + COL, initialPosition - 1);
            Console.Write(" ");
            Console.SetCursorPosition(initialPosition - 1, initialPosition + ROW);
            Console.Write(" ");
            Console.SetCursorPosition(initialPosition + COL, initialPosition + ROW);
            Console.Write(" ");

            for(int i=0;i<ROW;i++)
            {
                for(int j=0;j<COL;j++)
                {
                    if (Board[i, j] == 1)
                        Console.BackgroundColor = ConsoleColor.Red;
                    else
                        Console.BackgroundColor = BACKGROUND_COLOR;
                    Console.SetCursorPosition(initialPosition + j, initialPosition + i);
                    Console.Write(" ");


                }
            }
            

        }

        public static void DisplayScore()
        {
            int x, y;
            x = initialPosition + ROW/2;
            y = initialPosition + COL + 10;
            Console.SetCursorPosition(y, x);
            Console.BackgroundColor = ConsoleColor.DarkGray;
            Console.Write("Score: " + m_iScore);
        }

        public static void Log(string s)
        {
            s += "\r\n";
            System.IO.File.AppendAllText(@"\ConsoleTetrisLog.txt", s);
        }
            
        

        static void SetBackgroundColor()
        {
            Console.BackgroundColor = BACKGROUND_COLOR;
            Console.Clear(); //Important!
        }
    }
    class ObjectPattern
    {
        public int X; //Left
        public int Y; //Top
        public int iPattern;
        public int iDirection;
        public int[, ,] DesignArray = new int[4,3,3];
        public bool bDead;
        public int iMoveCounter;
        public ObjectPattern(int a, int d)
        {
            this.iPattern = a;
            this.iDirection = d;
            this.DesingObjectStructure(a);
            this.bDead = false;
            this.iMoveCounter = 0;

        }

        public void DesingObjectStructure(int a)
        {
            if(a==1)
            {
                DesignArray[0, 0, 0] = 0; DesignArray[0, 0, 1] = 0; DesignArray[0, 0, 2] = 0;
                DesignArray[0, 1, 0] = 1; DesignArray[0, 1, 1] = 1; DesignArray[0, 1, 2] = 1;
                DesignArray[0, 2, 0] = 0; DesignArray[0, 2, 1] = 1; DesignArray[0, 2, 2] = 0;

                DesignArray[1, 0, 0] = 0; DesignArray[1, 0, 1] = 1; DesignArray[1, 0, 2] = 0;
                DesignArray[1, 1, 0] = 1; DesignArray[1, 1, 1] = 1; DesignArray[1, 1, 2] = 0;
                DesignArray[1, 2, 0] = 0; DesignArray[1, 2, 1] = 1; DesignArray[1, 2, 2] = 0;

                DesignArray[2, 0, 0] = 0; DesignArray[2, 0, 1] = 1; DesignArray[2, 0, 2] = 0;
                DesignArray[2, 1, 0] = 1; DesignArray[2, 1, 1] = 1; DesignArray[2, 1, 2] = 1;
                DesignArray[2, 2, 0] = 0; DesignArray[2, 2, 1] = 0; DesignArray[2, 2, 2] = 0;

                DesignArray[3, 0, 0] = 0; DesignArray[3, 0, 1] = 1; DesignArray[3, 0, 2] = 0;
                DesignArray[3, 1, 0] = 0; DesignArray[3, 1, 1] = 1; DesignArray[3, 1, 2] = 1;
                DesignArray[3, 2, 0] = 0; DesignArray[3, 2, 1] = 1; DesignArray[3, 2, 2] = 0;
            }
            if(a == 2)
            {
                DesignArray[0, 0, 0] = 0; DesignArray[0, 0, 1] = 0; DesignArray[0, 0, 2] = 0;
                DesignArray[0, 1, 0] = 1; DesignArray[0, 1, 1] = 1; DesignArray[0, 1, 2] = 1;
                DesignArray[0, 2, 0] = 0; DesignArray[0, 2, 1] = 0; DesignArray[0, 2, 2] = 1;

                DesignArray[1, 0, 0] = 0; DesignArray[1, 0, 1] = 1; DesignArray[1, 0, 2] = 0;
                DesignArray[1, 1, 0] = 0; DesignArray[1, 1, 1] = 1; DesignArray[1, 1, 2] = 0;
                DesignArray[1, 2, 0] = 1; DesignArray[1, 2, 1] = 1; DesignArray[1, 2, 2] = 0;

                DesignArray[2, 0, 0] = 1; DesignArray[2, 0, 1] = 0; DesignArray[2, 0, 2] = 0;
                DesignArray[2, 1, 0] = 1; DesignArray[2, 1, 1] = 1; DesignArray[2, 1, 2] = 1;
                DesignArray[2, 2, 0] = 0; DesignArray[2, 2, 1] = 0; DesignArray[2, 2, 2] = 0;

                DesignArray[3, 0, 0] = 0; DesignArray[3, 0, 1] = 1; DesignArray[3, 0, 2] = 1;
                DesignArray[3, 1, 0] = 0; DesignArray[3, 1, 1] = 1; DesignArray[3, 1, 2] = 0;
                DesignArray[3, 2, 0] = 0; DesignArray[3, 2, 1] = 1; DesignArray[3, 2, 2] = 0;

            }

            if (a == 3)
            {
                DesignArray[0, 0, 0] = 0; DesignArray[0, 0, 1] = 0; DesignArray[0, 0, 2] = 1;
                DesignArray[0, 1, 0] = 1; DesignArray[0, 1, 1] = 1; DesignArray[0, 1, 2] = 1;
                DesignArray[0, 2, 0] = 0; DesignArray[0, 2, 1] = 0; DesignArray[0, 2, 2] = 0;

                DesignArray[1, 0, 0] = 0; DesignArray[1, 0, 1] = 1; DesignArray[1, 0, 2] = 0;
                DesignArray[1, 1, 0] = 0; DesignArray[1, 1, 1] = 1; DesignArray[1, 1, 2] = 0;
                DesignArray[1, 2, 0] = 0; DesignArray[1, 2, 1] = 1; DesignArray[1, 2, 2] = 1;

                DesignArray[2, 0, 0] = 0; DesignArray[2, 0, 1] = 0; DesignArray[2, 0, 2] = 0;
                DesignArray[2, 1, 0] = 1; DesignArray[2, 1, 1] = 1; DesignArray[2, 1, 2] = 1;
                DesignArray[2, 2, 0] = 1; DesignArray[2, 2, 1] = 0; DesignArray[2, 2, 2] = 0;

                DesignArray[3, 0, 0] = 1; DesignArray[3, 0, 1] = 1; DesignArray[3, 0, 2] = 0;
                DesignArray[3, 1, 0] = 0; DesignArray[3, 1, 1] = 1; DesignArray[3, 1, 2] = 0;
                DesignArray[3, 2, 0] = 0; DesignArray[3, 2, 1] = 1; DesignArray[3, 2, 2] = 0;

            }

            if (a == 4)
            {
                DesignArray[0, 0, 0] = 0; DesignArray[0, 0, 1] = 1; DesignArray[0, 0, 2] = 0;
                DesignArray[0, 1, 0] = 0; DesignArray[0, 1, 1] = 1; DesignArray[0, 1, 2] = 1;
                DesignArray[0, 2, 0] = 0; DesignArray[0, 2, 1] = 0; DesignArray[0, 2, 2] = 1;

                DesignArray[1, 0, 0] = 0; DesignArray[1, 0, 1] = 0; DesignArray[1, 0, 2] = 0;
                DesignArray[1, 1, 0] = 0; DesignArray[1, 1, 1] = 1; DesignArray[1, 1, 2] = 1;
                DesignArray[1, 2, 0] = 1; DesignArray[1, 2, 1] = 1; DesignArray[1, 2, 2] = 0;

                DesignArray[2, 0, 0] = 1; DesignArray[2, 0, 1] = 0; DesignArray[2, 0, 2] = 0;
                DesignArray[2, 1, 0] = 1; DesignArray[2, 1, 1] = 1; DesignArray[2, 1, 2] = 0;
                DesignArray[2, 2, 0] = 0; DesignArray[2, 2, 1] = 1; DesignArray[2, 2, 2] = 0;

                DesignArray[3, 0, 0] = 0; DesignArray[3, 0, 1] = 1; DesignArray[3, 0, 2] = 1;
                DesignArray[3, 1, 0] = 1; DesignArray[3, 1, 1] = 1; DesignArray[3, 1, 2] = 0;
                DesignArray[3, 2, 0] = 0; DesignArray[3, 2, 1] = 0; DesignArray[3, 2, 2] = 0;

            }

            if (a == 5)
            {
                DesignArray[0, 0, 0] = 0; DesignArray[0, 0, 1] = 0; DesignArray[0, 0, 2] = 0;
                DesignArray[0, 1, 0] = 1; DesignArray[0, 1, 1] = 1; DesignArray[0, 1, 2] = 1;
                DesignArray[0, 2, 0] = 0; DesignArray[0, 2, 1] = 0; DesignArray[0, 2, 2] = 0;

                DesignArray[1, 0, 0] = 0; DesignArray[1, 0, 1] = 1; DesignArray[1, 0, 2] = 0;
                DesignArray[1, 1, 0] = 0; DesignArray[1, 1, 1] = 1; DesignArray[1, 1, 2] = 0;
                DesignArray[1, 2, 0] = 0; DesignArray[1, 2, 1] = 1; DesignArray[1, 2, 2] = 0;

                DesignArray[2, 0, 0] = 0; DesignArray[2, 0, 1] = 0; DesignArray[2, 0, 2] = 0;
                DesignArray[2, 1, 0] = 1; DesignArray[2, 1, 1] = 1; DesignArray[2, 1, 2] = 1;
                DesignArray[2, 2, 0] = 0; DesignArray[2, 2, 1] = 0; DesignArray[2, 2, 2] = 0;

                DesignArray[3, 0, 0] = 0; DesignArray[3, 0, 1] = 1; DesignArray[3, 0, 2] = 0;
                DesignArray[3, 1, 0] = 0; DesignArray[3, 1, 1] = 1; DesignArray[3, 1, 2] = 0;
                DesignArray[3, 2, 0] = 0; DesignArray[3, 2, 1] = 1; DesignArray[3, 2, 2] = 0;

            }
            if (a == 6)
            {
                DesignArray[0, 0, 0] = 0; DesignArray[0, 0, 1] = 0; DesignArray[0, 0, 2] = 0;
                DesignArray[0, 1, 0] = 0; DesignArray[0, 1, 1] = 1; DesignArray[0, 1, 2] = 1;
                DesignArray[0, 2, 0] = 0; DesignArray[0, 2, 1] = 1; DesignArray[0, 2, 2] = 1;

                DesignArray[1, 0, 0] = 0; DesignArray[1, 0, 1] = 0; DesignArray[1, 0, 2] = 0;
                DesignArray[1, 1, 0] = 0; DesignArray[1, 1, 1] = 1; DesignArray[1, 1, 2] = 1;
                DesignArray[1, 2, 0] = 0; DesignArray[1, 2, 1] = 1; DesignArray[1, 2, 2] = 1;

                DesignArray[2, 0, 0] = 0; DesignArray[2, 0, 1] = 0; DesignArray[2, 0, 2] = 0;
                DesignArray[2, 1, 0] = 0; DesignArray[2, 1, 1] = 1; DesignArray[2, 1, 2] = 1;
                DesignArray[2, 2, 0] = 0; DesignArray[2, 2, 1] = 1; DesignArray[2, 2, 2] = 1;

                DesignArray[3, 0, 0] = 0; DesignArray[3, 0, 1] = 0; DesignArray[3, 0, 2] = 0;
                DesignArray[3, 1, 0] = 0; DesignArray[3, 1, 1] = 1; DesignArray[3, 1, 2] = 1;
                DesignArray[3, 2, 0] = 0; DesignArray[3, 2, 1] = 1; DesignArray[3, 2, 2] = 1;

            }
            
        }

        public void DisplayObject()
        {
            int x = X-1;
            int y = Y-1;
            int d = iDirection;
            int ix, jy;
            for(int i=0;i<3;i++)
            {
                ix = x + i - Program.initialPosition;

                for(int j=0;j<3;j++)
                {
                    jy = y + j - Program.initialPosition;
                    if(DesignArray[d,i,j] == 1)
                    {
                        Console.BackgroundColor = Program.OBJECT_COLOR;
                    }
                    else if(ix<0 || jy<0 || ix == Program.ROW || jy == Program.COL)
                    {

                        Console.BackgroundColor = ConsoleColor.Blue;
                        //Console.BackgroundColor = ConsoleColor.Red;
                    }
                    else if(Program.Board[ix,jy] == 1)
                    {
                        Console.BackgroundColor = ConsoleColor.Red;
                    }
                    else
                    {
                        Console.BackgroundColor = Program.BACKGROUND_COLOR;
                    }
                    Console.SetCursorPosition(y + j, x + i);
                    Console.Write(" ");
                }
            }
        }
        public void ChangeDirection()
        {
            if (bDead) return;
            if (!IsChangeDirectionPossible()) return;
            EraseObject();
            this.iDirection++;
            this.iDirection = iDirection % 4;
            DisplayObject();
        }

        public bool IsChangeDirectionPossible()
        {
            int x, y, ix, jy;
            x = X - 1;
            y = Y - 1;
            for (int i = 0; i < 3;i++ )
            {
                ix = x + i - Program.initialPosition;
                for(int j=0;j<3; j++)
                {
                    jy = y + j - Program.initialPosition;
                    if (ix < 0 || ix == Program.ROW || jy < 0 || jy == Program.COL) return false;

                }
            }
            return true;
        }

        public void EraseObject()
        {
            int x = X - 1;
            int y = Y - 1;
            int d = iDirection;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    
                    Console.BackgroundColor = Program.BACKGROUND_COLOR;
                    if (x + i - Program.initialPosition < 0 || x + i - Program.initialPosition == Program.ROW || y + j - Program.initialPosition < 0 || y + j - Program.initialPosition == Program.COL) continue;
                    Console.SetCursorPosition(y + j, x + i);
                    Console.Write(" ");
                }
            }
        }

        public void MoveCurrentObject(int x, int y)
        {
            if (bDead)
            {
                //EraseObject();
                return;
            }
            
            if (!CheckLastLine(x,y))
            {
                iMoveCounter++;
                EraseObject();
                X += x;
                Y += y;
                DisplayObject();
                return;
                
            }

            if(x ==1 && CheckLastLine(x, y) == true)
            {
                UpdateMainBoard();
            }
            

           

            

        }
        public bool CanMove(int x, int y)
        {
            if (bDead) return false;

            if (X-1+x < Program.initialPosition || X+1+x >= Program.ROW + Program.initialPosition)
                return false;

            if (Y-1+y < Program.initialPosition || Y+1+y >= Program.COL + Program.initialPosition)
                return false;

            return true;
        }
        public bool CheckLastLine(int fx, int fy)
        {
            bool isConflicted = false;
            int x, y;
            x = X + fx;
            y = Y + fy;

            x -= 1;
            y -= 1;

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    int ix, iy;
                    ix = x + i - Program.initialPosition;
                    iy = y + j - Program.initialPosition;
                    //Program.Log("Here Checking: DesignArray(i,j) = (2," + j + ") = " + DesignArray[iDirection, 2, j] + " && Board(i,j) = (" + ix + "," + iy + ") = " + Program.Board[ix, iy]);
                    if (DesignArray[iDirection, i, j] == 1 && (ix<0 || iy<0))
                    {
                        isConflicted = true;
                        break;
                    }
                    if (DesignArray[iDirection, i, j] == 1 && Program.Board[ix, iy] == 1)
                    {

                        isConflicted = true;
                        break;

                    }

                    /*
                    if (isConflicted == false && !isValid(ix - 1, iy))
                    {
                        Program.Log("IsValid returned false");
                        isConflicted = true;
                        break;
                    }
                     * */
                }
            }
            return isConflicted;
        }
        public void UpdateMainBoard()
        {
            int x, y;
            x = X-1;
            y = Y-1;
            for(int i=0;i<3;i++)
            {
                for(int j=0;j<3;j++)
                {
                    if(DesignArray[iDirection, i, j] == 1)
                    Program.Board[x + i - Program.initialPosition, y + j - Program.initialPosition] = DesignArray[iDirection, i, j];
                }
            }
            bDead = true;
            Program.DrawBoard();


        }
        public bool isValid(int x, int y)
        {
            Program.Log("Called for (x,y) = "+x +", "+y);
            if (x >= 0 && x < Program.ROW && y >= 0 && y < Program.COL)
                return true;
            return false;
        }
    }

    class GamePlay
    {
        // Volatile is used as hint to the compiler that this data 
        // member will be accessed by multiple threads. 
        private volatile bool bGameLoop;
        public void StartGame()
        {
            while (bGameLoop)
            {
                //Console.WriteLine("worker thread: working...");
                //Program.DrawBoard();
                if (Program.Obj.bDead == false)
                {
                    Program.Obj.DisplayObject();
                }
                else
                {
                    CheckAndUpdateAllRowForScore();
                    if(Program.Obj.iMoveCounter == 0)
                    {
                        RequestStop();
                        break;
                    }
                    Program.Log("Here is current Position of Board: ");
                    for (int i = 0; i < Program.ROW;i++ )
                    {
                        string str = "";
                        for(int j=0;j<Program.COL;j++)
                        {
                            str += Program.Board[i, j]+" ";
                        }
                        Program.Log(str);
                    }

                    Random rnd = new Random();
                    int iNext = rnd.Next(1, 7);
                    int iDirection = rnd.Next(0, 4);

                    Program.Obj = new ObjectPattern(iNext, iDirection);
                    Program.Obj.X = Program.initialPosition + 1;
                    Program.Obj.Y = Program.initialPosition + Program.COL/2;
                }
                Program.Obj.MoveCurrentObject(1, 0);
                Thread.Sleep(500);
            }
            //Console.WriteLine("worker thread: terminating gracefully.");
        }
        public void CheckAndUpdateAllRowForScore()
        {
            int iKount = 0;
            for(int i=0;i<Program.ROW;i++)
            {
                bool IsLineComplete = true;
                for(int j=0;j<Program.COL;j++)
                {
                    if (Program.Board[i,j] != 1)
                    {
                        IsLineComplete = false;
                        break;
                    }
                }
                if(IsLineComplete == true)
                {
                    iKount++;
                    for(int ii = i;ii>=0;ii--)
                    {
                        for(int jj=0;jj<Program.COL;jj++)
                        {
                            if(ii-1>=0)
                            Program.Board[ii, jj] = Program.Board[ii - 1, jj];
                        }
                    }
                }
            }
            Program.DrawBoard();
            Program.m_iScore += (iKount * iKount * 100);
            Program.DisplayScore();
        }
        public void RequestStop()
        {
            bGameLoop = false;
        }
        
        public void RequestStart()
        {
            bGameLoop = true;
        }

    }

}
